#ifndef __CSTDIO__
#define __CSTDIO__ 1
#include <cstdio>
#endif
#ifndef __CSTDBOOL__
#define __CSTDBOOL__ 1
#include <cstdbool>
#endif
#ifndef __IOSTREAM__
#define __IOSTREAM__ 1
#include <iostream>
#endif
#ifndef __STRING__
#define __STRING__ 1
#include <string>
#endif
#ifndef __STDEXCEPT__
#define __STDEXCEPT__ 1
#include <stdexcept>
#endif
#ifndef __GMP__
#define __GMP__ 1
#include <gmp.h>
#endif
#ifndef __MATH__
#define __MATH__ 1
#include <cmath>
#endif

#ifndef __FIEDELEM__
#define __FIELDELEM 1
#include "fieldelem.hpp"
#endif

#ifndef __NAMESPACE_STD__
#define __NAMESPACE_STD__ 1
using namespace std;
#endif 

/*
Implementation of SECP256K1 in C++

based on Jimmy Song's Programming Bitcoin
*/

class point
{
	public:
		
		signed long int a = 0;

		signed long int b = 0;

		signed long int x = 0;

		signed long int y = 0;

		point(long xin, long yin, long ain, long bin)
		{
			a = ain;

			b = bin;

			x = xin;

			y = yin;

			if ( x == 0 && y == 0 )
			{
				return;
			}

			if ( ( y * y ) != ( x * x * x ) + ( a * x ) + b )
			{
				throw std::runtime_error("Point (x,y) is not on curve secp256k1\n");

			}

		}

		~point()
		{
			cout << "Destroying point( " << x << "," << y << "," << a << "," << b <<  " )" << endl;	
		}

		void print(void)
		{
			cout << "Point(" << x << "," << y << "," << ")" << "_" << a << "_" << b << endl;
		}

		bool operator==(point other)
		{
			return x == other.x && y == other.y && a == other.a && b == other.b;
		}

		point * operator+( point other )
		{
			if ( a != other.a || b != other.b )
			{
				throw std::runtime_error("Points are not on the same curve\n");	
			}

			if ( x == 0 )
			{
				return new point(other.x,other.y,other.a,other.b);	
			}

			if ( other.x == 0 )
			{
				return new point(x,y,a,b);
			}

			if (
				( x == other.x )

				&&

				( -y == other.y )

			   )
			{
				return new point(0,0,a,b);
			}

			// When P_1 != P_2
			
			point * res = 0;

			if (
				(x != other.x)
			   )
			{
				signed long int s = (other.y - y)/(other.x - x);

				signed long int x3 = s * s - x - other.x;
				signed long int y3 = s * ( x - x3 ) - y;

				res = new point(x3,y3,a,b);				
			}

			else if (
					(x == other.x)

					&&

					(y == other.y)

				)
			{
				signed long int s = (3 * x * x + a) / ( 2 * y );
				signed long int x3 = s * s - 2 * x;
				signed long int y3 = s * (x - x3) - y;
				res = new point(x3,y3,a,b);	
			}

			return res;
		}

};

