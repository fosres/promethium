#ifndef __CSTDIO__
#define __CSTDIO__ 1
#include <cstdio>
#endif
#ifndef __CSTDBOOL__
#define __CSTDBOOL__ 1
#include <cstdbool>
#endif
#ifndef __IOSTREAM__
#define __IOSTREAM__ 1
#include <iostream>
#endif
#ifndef __STRING__
#define __STRING__ 1
#include <string>
#endif
#ifndef __STDEXCEPT__
#define __STDEXCEPT__ 1
#include <stdexcept>
#endif
#ifndef __GMP__
#define __GMP__ 1
#include <gmp.h>
#endif
#ifndef __MATH__
#define __MATH__ 1
#include <cmath>
#endif

#ifndef __FIELDELEM__
#define __FIELDELEM__ 1
#include "fieldelem.hpp"
#endif

#ifndef __NAMESPACE_STD__
#define __NAMESPACE_STD__ 1
using namespace std;
#endif 

/*
Implementation of SECP256K1 in C++

based on Jimmy Song's Programming Bitcoin
*/

class point
{
	public:
		
		fieldelem a;
		
		fieldelem b;
		
		fieldelem x;
		
		fieldelem y;

		point(fieldelem * ain, fieldelem * bin, fieldelem * xin, fieldelem * yin)
		{
			a = fieldelem(ain->num,ain->prime);
			
			b = fieldelem(bin->num,bin->prime);
			
			x = fieldelem(xin->num,xin->prime);
			
			y = fieldelem(yin->num,yin->prime);
/*
			if ( x == 0 && y == 0 )
			{
				throw std::runtime_error("Point (x,y) is at origin");
			}
*/
			if ( ( y^(2) ) != ( x^(3) ) + ( a * x ) + b )
			{
				throw std::runtime_error("Point (x,y) is not on curve secp256k1\n");

			}
/*
			if ( ( y * y ) != ( x * x * x  ) + ( a * x ) + b )
			{
				throw std::runtime_error("Point (x,y) is not on curve secp256k1\n");

			}
*/
		}
		
		point(unsigned long int ain, unsigned long int bin, unsigned long int xin, unsigned long int yin, unsigned long int prime)
		{
			a = fieldelem(ain,prime);
			
			b = fieldelem(bin,prime);
			
			x = fieldelem(xin,prime);
			
			y = fieldelem(yin,prime);
/*
			if ( x == 0 && y == 0 )
			{
				throw std::runtime_error("Point (x,y) is at origin");
			}
*/
			printf("Made it in\n");
			
			if ( ( y^(2) ) != ( ( ( x^(3) ) ) + ( a * x ) ) + b )
			{
				throw std::runtime_error("Point (x,y) is not on curve secp256k1\n");

			}
/*
			if ( ( y * y ) != ( ( x * x * x  ) + ( a * x ) + b ) )
			{
				throw std::runtime_error("Point (x,y) is not on curve secp256k1\n");


			}
*/
			printf("Made it out\n");
		}
/*
		~point()
		{
		
		}
*/
		void print(void)
		{
			gmp_printf("Point(%Zd,%Zd,)_%Zd_%Zd\n",x.num,y.num,a.num,b.num);

		}

		bool operator==(point other)
		{
			return x == other.x && y == other.y && a == other.a && b == other.b;
		}

		point operator+( point other )
		{

			if ( a != other.a || b != other.b )
			{
				throw std::runtime_error("Points are not on the same curve\n");	
			}

			if ( (*this) == other && y == ( x * 0 ) )
			{
				point ans = point(0x00,0x00,&a,&b);

				return ans;
			}

#if 0
			if ( x == 0 )
			{
				return new point(other.x,other.y,other.a,other.b);	
			}

			if ( other.x == 0 )
			{
				return new point(x,y,a,b);
			}
#endif
			if (
				( x == other.x )

				&&

				( y * (-1)  == other.y )
			   )
			{
				point ans = point(0x00,0x00,&a,&b);

				return ans;

			}

			// When P_1 != P_2
			

			if (
				( x != other.x )
			   )
			{
				fieldelem  s = ( (other.y) - (y) )/( other.x - x );

				fieldelem x3 = ( s^(2) ) - x - other.x;
				
			//	fieldelem x3 = s * s - x - other.x;

				fieldelem y3 = s * ( x - x3 ) - y;
			
				point res = point(&x3,&y3,&a,&b);				

				return res;

			}

			// When P1 == P2

			else if (
					( x == other.x )

					&&

					( y != other.y )

				)
			{
				fieldelem s = ( ( x^(2) ) * 3 + a ) / ( y * 2 );
				
			//	fieldelem s = ( x * x * 3 + a ) / ( y * 2 );

				fieldelem x3 = ( s^(2) ) - ( x * 2 );
				
			//	fieldelem x3 = s * s - ( x * 2 );

				fieldelem y3 = s * ( x - x3 ) - y;

				point res = point(&x3,&y3,&a,&b);	

				return res;

			}

		}

};

