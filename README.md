Compiling the Equihash algorithm:

g++ equihash.cpp -o /tmp/equihash.out -lsodium -fpermissive

NOTE: Requires downloading the LibSodium programming library: 

https://doc.libsodium.org/

Sample Command Line Usage:

/tmp/equihash.out 80 4

Sample Output:

0x5586c9742270

Solution strings in hexadecimal form

000000000000000008e8

00000000000000000f89

0000000000000000033b

00000000000000001380

00000000000000000a5d

000000000000000013bd

00000000000000000a56

00000000000000001717

000000000000000005cf

00000000000000000c0a

00000000000000000977

0000000000000000173b

000000000000000001c7

00000000000000001a48

00000000000000001435

00000000000000001ff7

verify boolean == 1

The "verify boolean == 1" is a print statement

made after calling the verify(). It returns 1

if and only if xoring all hashes of all solution

strings produces a final bit string of all 0

bits.
